﻿using System;

namespace ZExpressions.Keygen
{
    class Program
    {
        static void Main(string[] args)
        {
            var lic = LicenseInfo.Create("pedoc", new DateTime(2099, 12, 31), 400);

            var (licenseName, licenseKey) = lic.Generate();
            Console.WriteLine("licenseName=" + licenseName);
            Console.WriteLine("licenseKey=" + licenseKey);

            Console.ReadKey();
        }
    }
}
