﻿###### 分析

LicenseKey 满足以下逻辑
测试LicenseKey：111111111111111111111111111111111111111

1.LicenseKey如果以 "EFC;" 开头，则移除这几个字符后作为实际LicenseKey进行验证；
2.LicenseKey长度必须为36；
3.LicenseName组成结构如下
    数字;产品代码-授权人，示例：999;400-pedoc，如果产品代码为0，表示授权所有产品
4.LicenseKey组成结构如下
	由"-"分割
	
过期时间计算方法：
2位数字，代表年份，从2000年开始
2位数字，代表月份
2位数字，代表天数

如果过期时间为20991231，则LicenseKey中包含991231串


--直接看代码实现，难得写分析了 ：）
    
    



产品代码定义：
```{
			"100",
			".NET EF Extensions - Bundle"
		},
		{
			"101",
			".NET EF Extensions - SQL Server"
		},
		{
			"102",
			".NET EF Extensions - SQL Compact"
		},
		{
			"103",
			".NET EF Extensions - MySQL"
		},
		{
			"104",
			".NET EF Extensions - SQLite"
		},
		{
			"105",
			".NET EF Extensions - PostgreSQL"
		},
		{
			"106",
			".NET EF Extensions - Oracle"
		},
		{
			"107",
			".NET EF Extensions - Firebird"
		},
		{
			"300",
			".NET Bulk Operations - Bundle"
		},
		{
			"301",
			".NET Bulk Operations - SQL Server"
		},
		{
			"302",
			".NET Bulk Operations - SQL Compact"
		},
		{
			"303",
			".NET Bulk Operations - MySQL"
		},
		{
			"304",
			".NET Bulk Operations - SQLite"
		},
		{
			"305",
			".NET Bulk Operations - PostgreSQL"
		},
		{
			"306",
			".NET Bulk Operations - Oracle"
		},
		{
			"307",
			".NET Bulk Operations - Firebird"
		},
		{
			"400",
			"Compiler Expression.NET - Bundle"
		},
		{
			"500",
			"Eval Expression.NET"
		},
		{
			"600",
			"Eval SQL.NET"
		},
		{
			"700",
			"Dapper Plus - Bundle"
		},
		{
			"701",
			"Dapper Plus - SQL Server"
		},
		{
			"702",
			"Dapper Plus - SQL Compact"
		},
		{
			"703",
			"Dapper Plus - MySQL"
		},
		{
			"704",
			"Dapper Plus - SQLite"
		},
		{
			"705",
			"Dapper Plus - PostgreSQL"
		},
		{
			"706",
			"Dapper Plus - Oracle"
		},
		{
			"707",
			"Dapper Plus - Firebird"
		},
		{
			"900",
			"EF Classic - Bundle"
		},
		{
			"901",
			"EF Classic - SQL Server"
		},
		{
			"902",
			"EF Classic - SQL Compact"
		},
		{
			"903",
			"EF Classic - MySQL"
		},
		{
			"904",
			"EF Classic - SQLite"
		},
		{
			"905",
			"EF Classic - PostgreSQL"
		},
		{
			"906",
			"EF Classic - Oracle"
		},
		{
			"907",
			"EF Classic - Firebird"
		},
		{
			"950",
			"EF Classic - Standalone - Bundle"
		},
		{
			"951",
			"EF Classic - Standalone - SQL Server"
		}
```