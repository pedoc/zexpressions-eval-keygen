﻿using System;
using Z.Expressions;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            EvalManager.AddLicense("8842;400-pedoc", "0102FD109930D6003C007A000100E9000000");
            var licenseOk = EvalManager.ValidateLicense();

            var result = Eval.Execute("var x=1;return x;");
            Console.WriteLine("licenseOk=" + licenseOk);

            Console.ReadKey();
        }
    }
}
